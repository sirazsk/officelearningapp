import { createAsyncThunk, createSlice } from '@reduxjs/toolkit';
import studentService from '../services/student.service';
import { setMessage } from './message';

const initialState = {};

export const getSingleCourse = createAsyncThunk(
  '/student/getSingleCourse',
  async ({ courseId }, thunkAPI) => {
    try {
      //console.log(sectionId);
      const response = await studentService.getSingleCourse(courseId);
      thunkAPI.dispatch(setMessage(response.data.message));
      return response.data;
    } catch (error) {
      const message =
        (error.response &&
          error.response.data &&
          error.response.data.message) ||
        error.message ||
        error.toString();
      thunkAPI.dispatch(setMessage(message));
      return thunkAPI.rejectWithValue(message);
    }
  }
);

export const getAllCourses = createAsyncThunk(
  '/student/getallcourses',
  async (thunkAPI) => {
    try {
      //console.log(sectionId);
      const response = await studentService.getAllCourses();
      //thunkAPI.dispatch(setMessage(response.data.message));
      return response.data;
    } catch (error) {
      const message =
        (error.response &&
          error.response.data &&
          error.response.data.message) ||
        error.message ||
        error.toString();
      thunkAPI.dispatch(setMessage(message));
      return thunkAPI.rejectWithValue(message);
    }
  }
);

const studentSlice = createSlice({
  name: 'student',
  initialState,
  extraReducers: {
    [getSingleCourse.fulfilled]: (state, action) => {
      console.log('get single course fullfilled');
    },
  },
});

const { reducer, actions } = studentSlice;
export default reducer;
