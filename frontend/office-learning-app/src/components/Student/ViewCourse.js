import React from 'react';
import { useParams } from 'react-router-dom';

const ViewCourse = () => {
  const { id } = useParams();
  console.log(id);

  return (
    <div>
      <h1>view single course</h1>
    </div>
  );
};

export default ViewCourse;
