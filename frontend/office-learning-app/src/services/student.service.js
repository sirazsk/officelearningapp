import axios from 'axios';
import authHeader from './auth-header';

const API_URL = 'http://localhost:8080/';

//get single course details
const getSingleCourse = (courseId) => {
  return axios.get(API_URL + `student/courses/${courseId}`, {
    headers: authHeader(),
  });
};

//get all courses
const getAllCourses = () => {
  return axios.get(API_URL + 'student/courses', { headers: authHeader() });
};

const studentService = {
  getSingleCourse,
  getAllCourses,
};

export default studentService;
