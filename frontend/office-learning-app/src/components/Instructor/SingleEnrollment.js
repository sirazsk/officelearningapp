import React from 'react';

const SingleEnrollment = (obj) => {
  var enrollment = obj.enrollment;

  return (
    <div className='section'>
      <h1>{enrollment.myUser.fullName}</h1>
      <h3>{enrollment.id}</h3>
    </div>
  );
};

export default SingleEnrollment;
